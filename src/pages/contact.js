import React from 'react'
import HubbleLogo from '../images/hubble-white.svg'
import '../components/index.css'
import Layout from  '../components/Layout'
import styled from 'styled-components'
import CyclingSubHeading from '../components/CyclingSubHeading'
import AniLink from "gatsby-plugin-transition-link/AniLink"
import HeroImage from '../images/telescope.svg'

import SocialFooter from '../components/socialsFooter'

const FormContainer = styled.div`
    width: 70%;
    padding: 0 15%;
    @media (max-width: 1025px) { 
        width: 100%;
        padding: 0 8px;
    }
`

const SectionContainer = styled.section`
    height: 75vh;
    ${props => props.center ? `
        display: flex;
        flex-direction: column;
        justify-content: center;
    `: ''}
`


const DetailsContainer = styled.div`
    width: 30%;
    padding: 32px;
    color: rgb(42, 56, 141);
`

const ContactContainer = styled.section`
    display: flex;
    justify-content: center;
    margin-top: 96px;
    padding: 6em;
    @media (max-width: 1025px) { 
        margin-top: 16px;
        padding: 8px;
    }
`

const SubmitButton = styled.button`
    padding:0; 
    margin:0;
    border:0;
    background-color:transparent;
    color: rgb(42, 56, 141);
    font-weight: 500;
    font-size: 1.5em;
    margin-top: 32px;
    align-self: flex-start;
`

const TitleContainer = styled.div`
    display: flex;
    color: #D500F9;
    font-weight: 600;
    font-size: 2em;
    margin-bottom: 16px;
    @media (max-width: 1025px) { 
        flex-direction: column;
    }
`

const contactText = [' a website', ' to grow my business', ' a drink', ' content', ' a social strategy', ' a hug']

const IndexPage = () => {
    return (
    <Layout>
        <SectionContainer center={true}>
            <ContactContainer>
                <FormContainer>
                    <TitleContainer><span style={{marginRight: 12, color: 'rgb(42, 56, 141)'}}>I need</span><CyclingSubHeading titles={contactText}/></TitleContainer>
                    <form action="/success" name="Contact Form" method="POST" data-netlify="true">
                        <input type="hidden" name="form-name" value="Contact Form" />
                        <p style={{display: 'none'}}>
                            <label>Don’t fill this out if you're human: <input name="bot-field" /></label>
                        </p>
                        <div class="field">
                            <input type="text" name="fullname" id="fullname" placeholder="Jane Appleseed"/>
                            <label for="fullname">Name</label>
                        </div>
                        <div class="field">
                            <input type="text" name="email" id="email" placeholder="example@email.com"/>
                            <label for="email">Email</label>
                        </div>
                        <div class="field">
                            <input type="text" name="message" id="message" placeholder="Type your message here..."/>
                            <label for="message">Message</label>
                        </div>
                        <SubmitButton type="submit">Send ⟶</SubmitButton>
                    </form>
                </FormContainer>
            </ContactContainer>
        </SectionContainer>
    </Layout>
    )
}

export default IndexPage