import React from 'react'
import HubbleLogo from '../images/hubble-white.svg'
import '../components/index.css'
import Layout from  '../components/Layout'

import AniLink from "gatsby-plugin-transition-link/AniLink"

const IndexPage = () => {
    return (
    <Layout>
        Our Services
    </Layout>
    )
}

export default IndexPage