import React, {useEffect, useState} from 'react'
import HeroImage from '../images/telescope_animated_5.svg'
import HeroImageOld from '../images/telescope.svg'
import ContentImage from '../images/content.svg'
import '../components/index.css'
import Layout from  '../components/Layout'
import {useScrollPosition} from '../hooks/useScrollPosition'
import styled from 'styled-components'
import AniLink from "gatsby-plugin-transition-link/AniLink"
import Typed from 'react-typed';

import SocialFooter from '../components/socialsFooter'

import CyclingSubHeading from '../components/CyclingSubHeading'

import Client1 from '../images/savills.jpg'
import Client2 from '../images/2.svg'
import Client3 from '../images/3.svg'
import Client4 from '../images/4.png'
import Client5 from '../images/5.jpg'

import Client6 from '../images/logos.jpeg'
import Client7 from '../images/collective.png'

const HeroContainer = styled.div`
    height: ${props => props.eighty ? '70vh;' : '100vh;'}
    position: relative;
`
const ImageContainer = styled.div`
    width: 40%;
    height: 70%;
    position: absolute;
    right: 7%;
    top: 20%;
    bottom: 20%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    @media (max-width: 1025px) { 
        top: 15%;
        height: 40%;
        width: 80%;
    }
`
const TextContainer = styled.div`
    color: #999;
    position: absolute;
    left: 0px;
    top: 25%;
    font-weight: 600;
    font-size: 26px;
    line-height: 1.1;
    text-align: left;
    width: 630px;
    padding-left: 8%;
    @media (max-width: 1025px) { 
        top: auto;
        bottom: 25%;
        padding-left: 16px;
        width: calc(100% - 32px);
    }
`

const HeadingStatic = styled.h1`
    color: rgb(42, 56, 141);
    font-size: 72px;
    font-weight: bold;
    margin-right: 15px;
    @media (max-width: 1025px) { 
        font-size: 32px;
        margin-right: 8px;
    }
`
const HeadingChanging = styled.h1`
    color: rgb(42, 56, 141);
    font-size: 72px;
    font-weight: bold;
    width: 100%;
    @media (max-width: 1025px) { 
        font-size: 32px;
    }
`
const HeadingSub = styled(AniLink)`
    color: rgb(42, 56, 141);
    font-size: 32px;
    font-weight: normal;
    & : hover {
        color: #D500F9;
    }
`


const SectionContainer = styled.section`
    height: ${props => props.eighty ? '70vh;' : '100vh;'}
    ${props => props.notSmall ? '' : 'max-height: 600px;'}
    position: relative;
    ${props => props.center ? `
        display: flex;
        flex-direction: column;
        justify-content: center;
    `: ''}
`

const AnimatedTextLeft = styled.div`
    font-size: 12em;
    color: rgb(42, 56, 141);
    font-weight: bold;
    transform: translate(${props => props.translateAmount}px, 0px);
    transition: transform 0.2s;
    @media (max-width: 1025px) { 
        font-size: 4em;
        transform: translate(${props => props.translateAmount-600}px, 0px);
    }
    line-height: 1em;
`
const AnimatedTextRight = styled.div`
    font-size: 12em;
    color: #D500F9;
    font-weight: bold;
    text-align: right;
    transform: translate(${props => props.translateAmount}px, 0px);
    transition: transform 0.2s;
    @media (max-width: 1025px) { 
        font-size: 4em;
        transform: translate(${props => props.translateAmount+600}px, 0px);
    }
    line-height: 1em;
`

const InfoTextContainer = styled.div`
    display: flex;
    flex-direction: row-reverse;
    justify-content: space-between;
    align-items: stretch;
    padding: 32px;
    @media (max-width: 1025px) { 
        flex-direction: column;
        padding: 8px;
        padding-top: 96px;
    }
`
const InfoTextLeft = styled.div`
    color: #D500F9;
    font-size: 2.2em;
    padding: 64px;
    width: 50%;
    transition: opacity 0.5s;
    opacity: ${props => props.show ? '1' : '0'};
    @media (max-width: 1025px) { 
        width: calc(100% - 16px);
        padding: 8px;
        font-size: 1.4em;
    }
`
const InfoTextRight = styled.div`
    color: rgb(42, 56, 141);
    font-size: 1.5em;
    padding: 64px;
    width: 50%;
    transition: opacity 0.5s 0.2s;
    opacity: ${props => props.show ? '1' : '0'};
    @media (max-width: 1025px) { 
        width: calc(100% - 16px);
        padding: 8px;
        font-size: 1.4em;
    }
`

const GoodComapny = styled.div`
    color: rgb(42, 56, 141);
    font-size: 3em;
    font-weight: 700;
    margin: 32px 96px;
    margin-bottom: 32px;
`

const PartnerContainer = styled.div`
    margin: 0 -32px;
    display: flex;
    align-items: center;
    justify-content: center;
    @media (max-width: 1025px) { 
        height: auto;
        margin: 0 8px;
        align-items: center;
        flex-direction: column-reverse;
    }
`
const PartnerLeftContainer = styled.div`
    background: white;
    padding: 48px;
    width: 75%;
    font-size: 2.3em;
    display: flex; 
    align-items: center;
    padding-left: 128px;
    @media (max-width: 1025px) { 
        font-size: 1.5em;
        padding-left: 32px;
        width: 100%;
        padding: 8px;
    }
`
const PartnerRightContainer = styled.div`
    background: rgb(42, 56, 141);
    background: white;
    padding: 48px 64px;
    width: 25%;
    font-size: 3.5em;
    color: #D500F9;
    display: flex; 
    align-items: center;
    padding-right: 96px;
    @media (max-width: 1025px) { 
        padding: 48px;
        width: 100%;
        height: 200px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
`

const DriveGrowth = styled.div`
    padding: 96px;
    padding-right: 30%;
    font-size: 4em;
    color: rgb(42, 56, 141);
    @media (max-width: 1025px) { 
        padding: 8px;
        padding-right: 5%;
        font-size: 1.5em;
    }
`

const WeDo = styled.div`
    padding: 48px 96px;
    color: #D500F9;
    font-size: 10em;
    font-weight: 600;
    transform: translate(${props => props.show ? '0' : '-700'}px, 0px);
    transition: transform 1s;
`

const ServicesContainer = styled.div`
    display: flex;
    margin: 16px 96px;
    @media (max-width: 1025px) { 
        margin: 0px 0px;
        flex-direction: column;
    }
`

const ServicesTitleContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 40%;
    @media (max-width: 1025px) { 
        width: 100%;
        flex-direction: row;
        justify-content: space-evenly;
        align-items: center;
    }
`

const ServicesTitle = styled.span`
    font-size: ${props => props.selected ? '2.7em' : '2.5em'};
    font-weight: ${props => props.selected ? '700' : '500'};
    margin: 16px 0;
    color: ${props => props.selected ? '#D500F9' : 'rgb(42, 56, 141)'};
    @media (max-width: 1025px) { 
        font-size: 0.8em;
        margin: 0;
        text-align: center;
        width: 25%;
        padding: 8px;
    }
`

const ServicesDescriptionContainer = styled.div`
    width: 60%;
    @media (max-width: 1025px) { 
        width: 100%;
    }
`
const ServicesDescriptionInnerContainer = styled.div`
    margin: 32px;
    padding: 16px;
    border-top: 1px solid rgb(42, 56, 141);
    color: rgb(42, 56, 141);
    display: flex;
    height: 250px;
`

const ServicesDescriptionInnerLeft = styled.div`
    margin: 8px;
    width: 70%;
    color: rgb(42, 56, 141);
    padding-right: 32px;
    font-size: 2em;
    @media (max-width: 1025px) { 
        font-size: 0.8em;
    }
`
const ServicesDescriptionInnerRight = styled.div`
    margin: 8px;
    width: 30%;
    @media (max-width: 1025px) { 
        font-size: 0.8em;
    }
`
const SepLine = styled.div`
    border-top: 1px solid #eeeeee;
    margin: 0 -32px;
`

const CenterTextSect = styled.div`
    color: #D500F9;
    text-align: center;
    font-size: 3em;
    font-weight: 600;
`

const StyledAniLink = styled(AniLink)`
    color: #D500F9;
    text-decoration: underline;
    & : hover {
        color: #D500F9;
    }
`
const HeadingContainers = styled.div`
    display: flex;
`

const PartnerImage = styled.img`
    max-width: 100%; 
    max-height: 100%;
    width: 100%;
    object-fit: contain;
    @media (max-width: 1025px) { 
        width: auto;
        height: 100%;
    }
`

const partnerNames = ['CBRE', 'GPT', 'LOGOS', 'SAVILLS', 'SYDNEY BUSINESS PARK', 'COMMERCIAL COLLECTIVE', 'CHARTER HALL']
const parnterImages = [Client5, Client2, Client6, Client1, Client4, Client7, Client3]
const serviceNames = ['Strategy', 'Content', 'Web & Tech', 'Performance Marketing']

const serviceDescs = [
    (<> There is a lot of noise out there. We take a calculated approach to help you be heard, be seen, and be ahead of the rest. </>),
    (<> More than simply looking good, we create content that’s focused purely on producing results. Creativity that works. </>),
    (<> Make the web work for you. Our approach to digital harnesses the power of technology to deliver active engagement. </>),
    (<> Content is setting up the shot; performance marketing is the slam dunk. We’ll show you how to achieve your goals. </>),
]

const serviceDeliverables = [
    (<> 
        Research & Data <br/>
        Branding & Positioning <br/>
        Campaign Planning <br/>
        Social Media Strategy <br/>
        User Experience <br/>
        Prototyping
    </>),
    (<>
        Copywriting<br/>
        Social Media<br/>
        Document Design<br/>
        Motion Design<br/>
        Illustration<br/>
        Video<br/>
        HTML5 Production
    </>),
    (<>
        Landing pages <br/>
        Web Development <br/>
        Marketing Automation
    </>),
    (<>
        SEO <br/>
        Paid Social <br/>
        Paid Search <br/>
        Search Engine Marketing <br/>
        Marketing Automation
    </>),
]



const IndexPage = () => {
    const [scrollAmount, setScrollAmount] = useState(1)
    let [screenWidth, setScreenWidth] = useState(0)
    let [screenHeight, setScreenHeight] = useState(0)
    let [selectedPartner, setSelectedPartner] = useState(0)
    let [selectedServices, setSelectedService] = useState(0);
    useScrollPosition(({ currPos, prevPos }) => {
        setScrollAmount(currPos.y)
    }, [scrollAmount], null, true, 40)
    useEffect(() => {
        setScreenWidth(window.innerWidth)
        setScreenHeight(window.innerHeight)
        window.addEventListener("resize", function() {
          setScreenWidth(window.innerWidth)
          setScreenHeight(window.innerHeight)
        });
        setInterval(() => {
            setSelectedPartner(prev => {
                if (prev >= partnerNames.length -1) {
                    return 0
                }
                return prev + 1
            })
        }, 3000);
    }, [])

    return (
    <Layout>
        <HeroContainer>
            <ImageContainer> <img style={{width: '100%'}} src={HeroImage}/> </ImageContainer>
            <TextContainer> 
                <HeadingContainers>
                    <HeadingStatic> Strategic </HeadingStatic>
                    <HeadingChanging> <CyclingSubHeading titles={['Content', 'Digital', 'Marketing', 'Technology']}/> </HeadingChanging>
                </HeadingContainers>
                <HeadingContainers>
                    <HeadingStatic> Driving </HeadingStatic>
                    <HeadingChanging> <CyclingSubHeading offset={true} titles={['Growth', 'Sales', 'Leads', 'Engagement', 'Results']}/> </HeadingChanging>
                </HeadingContainers>
            </TextContainer>
        </HeroContainer>
        <SectionContainer center={true} eighty={true}> 
            <DriveGrowth> We drive growth and transformation for our clients through strategy, design, content, and technology. </DriveGrowth>
        </SectionContainer>
        <div> 
            <AnimatedTextRight translateAmount={-1200 + (scrollAmount/2)}> Taking a </AnimatedTextRight>
            <AnimatedTextLeft translateAmount={1100 - (scrollAmount/2)}> closer look </AnimatedTextLeft>
        </div>
        <div style={{margin: '64px 0'}}> 
            <InfoTextContainer> 
                <InfoTextRight show={scrollAmount >= screenHeight*1.6}> 
                    <p> Led by Mark Cronje, Hubble combines years of expertise in the delivery of complex strategy, design, content, app and web projects in a range of fast-moving industries from commercial property to retail and beyond. </p>
                    <br/>
                    <p> We offer a rare clarity and depth of understanding, with the resources and tools to drive meaningful growth for clients and measurable results for campaigns.</p> 
                </InfoTextRight>
                <InfoTextLeft show={scrollAmount >= screenHeight*1.6}> Hubble is a Melbourne-based consultancy combining strategy, design, content and technology. We bring an integrated approach to streamline and improve your marketing. The result? More focus. More results. </InfoTextLeft>
            </InfoTextContainer>
        </div>
        <SectionContainer center={true} eighty={true}>
            {/* <WeDo show={-scrollAmount >= screenHeight*2}> we do </WeDo> */}
            <ServicesContainer>
                <ServicesTitleContainer> 
                    {serviceNames.map((name, key) => (<ServicesTitle selected={key === selectedServices} onMouseEnter={() => setSelectedService(key)}> {name} </ServicesTitle>))}
                </ServicesTitleContainer>
                <ServicesDescriptionContainer> 
                    <ServicesDescriptionInnerContainer>
                        <ServicesDescriptionInnerLeft> {serviceDescs[selectedServices]} </ServicesDescriptionInnerLeft>
                        <ServicesDescriptionInnerRight> {serviceDeliverables[selectedServices]} </ServicesDescriptionInnerRight>
                    </ServicesDescriptionInnerContainer>
                </ServicesDescriptionContainer>
            </ServicesContainer>
        </SectionContainer>
        <SectionContainer eighty={true} center={true} > 
            <PartnerContainer> 
                <span style={{fontWeight: 600, fontSize: 24, color: '#D500F9'}}> Trusted by industry leaders </span>
            </PartnerContainer>
            <br/><br/>
            <PartnerContainer style={{height: 60}}> 
                {/* <PartnerLeftContainer> <span>{partnerNames.map((name,key) => <span style={{color: 'rgb(42, 56, 141)'}}><span style={{fontWeight: (key === selectedPartner ? '600' : '400'), color:  (key === selectedPartner ? 'rgb(42, 56, 141)' : 'rgb(42, 56, 141)')}}> {name}</span>{key !== partnerNames.length-1 ? ',': ''} </span> )} </span> </PartnerLeftContainer>
                <PartnerRightContainer> <PartnerImage src={parnterImages[selectedPartner]} /></PartnerRightContainer> */}
                {parnterImages.map((val,key) =>{
                    return <img style={{height: selectedPartner === key ? 42 : 28, margin: '8px 24px', transition: '0.3s height'}} src={val}/>
                } )}
            </PartnerContainer> 
        </SectionContainer>
        <SectionContainer style={{marginBottom: 48}} eighty={true} notSmall={true}> 
            <HeroContainer eighty={true}>
                <ImageContainer> <img style={{width: '100%', marginRight: -64, marginTop: -32}} src={ContentImage}/> </ImageContainer>
                <TextContainer> 
                    <HeadingChanging> Let's take a <br/> closer look </HeadingChanging>
                    <br/>
                    <HeadingSub paintDrip duration={0.6} direction="left" hex="#D500F9" to="contact"> <span style={{marginLeft: 5}}>Talk to us</span> <span style={{color: '#D500F9'}}> ⟶ </span></HeadingSub>
                </TextContainer>
            </HeroContainer> 
        </SectionContainer>
    </Layout>
    )
}

export default IndexPage

//     <ConsultationContainer>
//     <ConsultationContainerUnskew>
//         <ConsultationHeadText> Get in touch? </ConsultationHeadText>
//         <ConsultationBodyText>
//             Want a free thirty minute consultation to hear how we can help you?
//         </ConsultationBodyText>
//         <a href="https://meetings.hubspot.com/mark726"><BookNowButton> BOOK NOW → </BookNowButton></a>
//     </ConsultationContainerUnskew>
// </ConsultationContainer>
// <AboutContainer>
//     <Skew style={{display: 'flex', alignItems: 'stretch', margin: 8}}>
//         <div style={{width: '20%', border: 'solid black 1px', margin: 8, minWidth: 260, overflow: 'hidden'}}> <UnSkew> <img src="https://external-preview.redd.it/miCQCnXu7j80OK7H4X72uZUfAcJsa8J2jaoMGxw4R1Q.jpg?auto=webp&s=2554cf5c77eaf8197b0d2642ec16568c6f563f9f" style={{ width: '400px'}}/> </UnSkew> </div>
//         <div style={{display: 'flex', alignItems: 'stretch', flexDirection: 'column', flexGrow: 2}} >
//             <div style={{display: 'flex', alignItems: 'stretch'}}> 
//                 <div style={{flexGrow: 2, border: 'solid black 1px', margin: 8, padding: '1.5em'}}> <UnSkew> askdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjh askdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjh askdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjh   </UnSkew> </div>
//                 <div style={{border: 'solid black 1px', margin: 8, padding: '1.5em', minWidth: 196}}> <UnSkew> <ClientImage src={HeroImage}/> </UnSkew></div>
//             </div>
//             <div style={{border: 'solid black 1px', margin: 8, padding: '1.5em'}}> <UnSkew> askdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjh askdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjh askdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjhaskdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjh askdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjh  askdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjh </UnSkew> </div>
//         </div> 
//     </Skew>
// </AboutContainer>
// <ClientImageContainer>
//     <ClientImage src={Client1}/>
//     <ClientImage src={Client2}/>
//     <ClientImage src={Client3}/>
//     <ClientImage style={{background: '#000'}} src={Client4}/>
//     <ClientImage src={Client5}/>
// </ClientImageContainer>
// <FeedContainer>
//     <FeedItemContainer>
//         <FeedItemImage src="https://images.pexels.com/photos/267389/pexels-photo-267389.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"/>
//         <FeedItemText> 
//             <h3> sdkjhafdskhj </h3>
//             askdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjh askdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjh askdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjh 
//         </FeedItemText>
//     </FeedItemContainer>
//     <FeedItemContainerReverse>
//         <FeedItemImage src="https://images.pexels.com/photos/1841120/pexels-photo-1841120.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"/>
//         <FeedItemText> 
//             <h3> sdkjhafdskhj </h3>
//             askdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjh askdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjh askdfjhklasdhflkasj afsdlkjh asdkjfh aslkdf aslkdfh lkasjdhf lkajsdhf lkasjdhf laksdfjh 
//         </FeedItemText>           
//     </FeedItemContainerReverse>
// </FeedContainer>