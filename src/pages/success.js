import React from 'react'
import HubbleLogo from '../images/hubble-white.svg'
import '../components/index.css'
import Layout from  '../components/Layout'
import styled from 'styled-components'
import CyclingSubHeading from '../components/CyclingSubHeading'
import AniLink from "gatsby-plugin-transition-link/AniLink"
import HeroImage from '../images/telescope.svg'

import SocialFooter from '../components/socialsFooter'

const HeadingSub = styled(AniLink)`
    color: rgb(42, 56, 141);
    font-size: 32px;
    font-weight: normal;
    width: 100%;
    display: block;
    text-align: center;
    color: #D500F9;
    & : hover {
        color: #D500F9;
    }
`

const FormContainer = styled.div`
    width: 70%;
    padding: 0 15%;
    @media (max-width: 1025px) { 
        width: 100%;
        padding: 0 8px;
    }
`

const SectionContainer = styled.section`
    height: 75vh;
    ${props => props.center ? `
        display: flex;
        flex-direction: column;
        justify-content: center;
    `: ''}
`


const DetailsContainer = styled.div`
    width: 30%;
    padding: 32px;
    color: rgb(42, 56, 141);
`

const ContactContainer = styled.section`
    display: flex;
    justify-content: center;
    margin-top: 96px;
    padding: 6em;
    @media (max-width: 1025px) { 
        margin-top: 16px;
        padding: 8px;
    }
`

const SubmitButton = styled.button`
    padding:0; 
    margin:0;
    border:0;
    background-color:transparent;
    color: rgb(42, 56, 141);
    font-weight: 500;
    font-size: 1.5em;
    margin-top: 32px;
    align-self: flex-start;
`

const TitleContainer = styled.div`
    display: flex;
    color: #D500F9;
    font-weight: 600;
    font-size: 2em;
    margin-bottom: 16px;
    @media (max-width: 1025px) { 
        flex-direction: column;
    }
`

const contactText = [' a website', ' to grow my business', ' a drink', ' content', ' a social strategy', ' a hug']

const IndexPage = () => {
    return (
    <Layout>
        <SectionContainer center={true}>
            <ContactContainer>
                <FormContainer>
                    <TitleContainer><span style={{color: 'rgb(42, 56, 141)', width: '100%', textAlign: 'center'}}>Thanks for your enquiry. <br/> We'll be in touch shortly </span></TitleContainer>
                    <HeadingSub to="/" paintDrip duration={0.6} direction="left" hex="#D500F9" > Home</HeadingSub>
                </FormContainer>
            </ContactContainer>
        </SectionContainer>
    </Layout>
    )
}

export default IndexPage