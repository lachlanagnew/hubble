import React from 'react'
import styled from 'styled-components'

import SocialFooter from './socialsFooter'

const TopFooter = styled.div`
`

const BottomFooter = styled.div`
    padding: 0rem 2.5rem;
    font-size: .875rem;
    font-weight: 400;
    display: flex;
    justify-content: space-between;
    background: white;
    color: black;
    @media (max-width: 1025px) { 
        padding: 0rem 0.5rem;
        align-items: flex-end;
        font-size: .5rem;
    }
`

const FooterContainer = styled.div`
    padding: 1.5rem;
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
` 

const FooterHeadText = styled.span`
    font-size: 4rem;
    line-height: 1.125;
    font-weight: 700;
    color: white;
    margin-bottom: 5rem;
    font-size: 64px;
`

const FooterElement = styled.span`
    width: 33%;
    text-align: center;
`

const Header = () => {
    return (
        <FooterContainer>
            <TopFooter  >
                <SocialFooter/>
            </TopFooter>
            <BottomFooter> 
                <FooterElement> <a style={{color: '#D500F9'}} href="mailto://hello@hubblegroup.com.au">hello@hubblegroup.com.au</a> </FooterElement>
                <FooterElement style={{color: '#D500F9'}}> Hubble Australia © 2020 </FooterElement>
                <FooterElement style={{color: '#D500F9'}}> Level 5/414 Lonsdale St, Melbourne VIC 3000</FooterElement>
            </BottomFooter>
        </FooterContainer>
    )
}

export default Header