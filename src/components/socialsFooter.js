import React from 'react'
import styled from 'styled-components'


const BottomFooter = styled.div`
    padding: 0rem 2.5rem;
    font-size: 1.2rem;
    font-weight: 700;
    display: flex;
    justify-content: space-between;
    background: white;
    color: black;
`

const FooterContainer = styled.div`
    padding: 1.5rem;
    padding-top: 0;
` 

const FooterHeadText = styled.span`
    font-size: 4rem;
    line-height: 1.125;
    font-weight: 700;
    color: white;
    margin-bottom: 5rem;
    font-size: 64px;
`

const StyledLink = styled.a`
    color: rgb(42, 56, 141);
    margin: 0 8px;
`

const Header = () => {
    return (
        <FooterContainer>
            <BottomFooter> 
                <span> </span>
                <span>
                    <StyledLink target="_blank" href="https://www.facebook.com/hubbleaustralia"> <i class="fab fa-facebook-f"></i> </StyledLink> 
                    <StyledLink target="_blank" href="https://www.instagram.com/hubblegroupau"> <i class="fab fa-instagram"></i> </StyledLink>
                    <StyledLink target="_blank" href="https://www.linkedin.com/company/hubblegroup"> <i class="fab fa-linkedin-in"></i> </StyledLink>
                </span>
                <span> </span>
            </BottomFooter>
        </FooterContainer>
    )
}

export default Header