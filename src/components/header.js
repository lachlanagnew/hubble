import React, {useState, useEffect} from 'react'
import styled from 'styled-components'
import Logo from '../images/hubble.svg'
import Menu from '../images/menu.svg'
import AniLink from "gatsby-plugin-transition-link/AniLink"

const HeaderContainer = styled.div`
    background: white;
    transition-delay: 1.5s;
    position: fixed;
    width: 100%;
    top: 0;
    line-height: 1;
    z-index: 30;
    opacity: 1;
    transition: opacity .75s linear,transform 1.2s cubic-bezier(.245,.495,0,.99);
    padding-bottom: 1.125rem;
    padding-top: 1.65rem;
    padding-left: 3rem;
    padding-right: 3rem;
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
`

const StyledLogo = styled.img`
    height: 32px;
    margin-left: -2px;
`
const StyledMenu = styled.img`
    width: 28px;
`

const StyledAniLink = styled(AniLink)`
    color: rgb(42, 56, 141);
    font-weight: 600;
    margin: 0 16px;
    font-size: 1.5em;
    & : hover {
        color: #D500F9;
    }
`

const Header = () => {
    const [screenWidth, setScreenWidth] = useState(undefined)
    useEffect(()=> {
        if (typeof window !== 'undefined') {
            setScreenWidth(window.innerWidth)
            window.addEventListener("resize", function() {
              setScreenWidth(window.innerWidth)
            });
        }
    }, [])
    return <>
        {
            <HeaderContainer>
                <AniLink paintDrip duration={0.6} direction="left" hex="#D500F9" to="/"> <StyledLogo src={Logo}/> </AniLink>
                <span style={{marginBottom: 3}}>
                    <StyledAniLink style={{marginRight: 0}} paintDrip duration={0.6} direction="left" hex="#D500F9" to="contact"> Say hello </StyledAniLink>
                </span>
            </HeaderContainer>
        }

    </>
}

export default Header