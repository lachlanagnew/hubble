import React from 'react'
import { Helmet } from 'react-helmet'
import './all.sass'
import '../fonts/stylesheet.css'
import useSiteMetadata from './SiteMetadata'
import Header from './header'
import Footer from './footer'

import Favicon from '../images/favicon64.png'
import Banner from '../images/og.jpeg'


const TemplateWrapper = ({ children }) => {
  const { title, description } = useSiteMetadata()
  return (
    <div style={{minHeight: '92vh'}}>
      <Helmet>
        <html lang="en" />
        <title>{title}</title>
        <meta name="description" content={description} />
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"/>

        <meta name="theme-color" content="#fff" />

        <link rel="icon" type="image/png" href={Favicon} sizes="64x64" />

        <meta property="og:url" content="/" />
        <meta property="og:image" content={Banner} />
        <meta property="og:type" content="business.business" />
        <meta property="og:title" content={title} />

        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
        <meta property="twitter:image" content={Banner} />
      </Helmet>
      <Header/>
      <div style={{marginTop: 64, padding: '0 1.5rem'}}>{children}</div>
      <Footer/>
    </div>
  )
}

export default TemplateWrapper
