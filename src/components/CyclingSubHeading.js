import React, {useState, useEffect} from 'react'
import styled from 'styled-components'

const CyclingSubHeadingContainer = styled.div`
    overflow: hidden;
`
const CyclingSubHeadingText = styled.div`
    transition: transform 0.3s;
    transform: translate(0px, ${props => props.offset}px);
`

const CyclingSubHeading = ({titles, offset}) => {
    let [selected, setSelected] = useState(0)
    let [animationState, setAnimationState] = useState(false)
    useEffect(() => {
        if (offset === true) {
            setTimeout(() => {
                setInterval(() => {
                    setAnimationState(true)
                    setTimeout(() => {
                        setSelected(prev => {
                            if (prev >= titles.length -1) {
                                return 0
                            }
                            return prev + 1
                        })
                    }, 300)
                    setTimeout(() => {
                        setAnimationState(false)
                    }, 400)
                }, 3000);
            }, 1500)
        } else {
            setInterval(() => {
                setAnimationState(true)
                setTimeout(() => {
                    setSelected(prev => {
                        if (prev >= titles.length -1) {
                            return 0
                        }
                        return prev + 1
                    })
                }, 300)
                setTimeout(() => {
                    setAnimationState(false)
                }, 400)
            }, 3000);
        }
        
       
    }, [])

    return (
        <CyclingSubHeadingContainer> <CyclingSubHeadingText offset={animationState ? 100: 0}> {titles[selected]} </CyclingSubHeadingText> </CyclingSubHeadingContainer>
    )
}

export default CyclingSubHeading